export interface AutogrowOptions {
  /**
   * Don't add another char width at the end of the input
   */
  snug?: boolean;
}

/**
 * make a input element automatically adjust its size to its content
 * @param input
 * @param options
 */
function autogrow(input: HTMLInputElement, options: AutogrowOptions = {}) {
  if (!input) {
    return {
      update(options?: AutogrowOptions) {},
      destroy() {},
    };
  }

  const measurementSpan = createMeasurementSpan();

  return control(input, measurementSpan, options);
}

export default autogrow;

function control(
  input: HTMLInputElement,
  measurementSpan: HTMLSpanElement,
  options: AutogrowOptions
) {
  const events = ["change", "paste", "input", "keydown"];

  events.forEach((ev) => input.addEventListener(ev, handleChange));

  const observer = makeObserver(handleChange);
  const observerOptions = {
    attributes: true,
    attributeOldValue: true,
    attributeFilter: ["placeholder", "style"],
  };
  observer.observe(input, observerOptions);

  function handleChange() {
    observer.disconnect();
    update(options);
    observer.observe(input, observerOptions);
  }

  function update(newOptions?: AutogrowOptions) {
    options = newOptions ?? options;

    if (!measurementSpan.parentNode) {
      throw new Error("autogrow has already been destroyed");
    }

    const { value, placeholder } = input;

    // the already used space of borders and padding
    const spacing = Math.abs(
      (input.clientWidth ?? 0) - input.getBoundingClientRect().width
    );

    const inputStyle = getComputedStyle(input);

    // make sure that the span has he same styling as the input
    // to get the correct width, that will be applied to the input
    measurementSpan.style.fontSize = inputStyle.fontSize;
    measurementSpan.style.fontFamily = inputStyle.fontFamily;
    measurementSpan.style.letterSpacing = inputStyle.letterSpacing;
    measurementSpan.style.textTransform = inputStyle.textTransform;

    measurementSpan.textContent = value || placeholder || " ";

    const spanWidth = measurementSpan.getBoundingClientRect().width;

    // determine the width of a single char
    const charWidth =
      value || placeholder
        ? spanWidth / ((value.length || placeholder?.length) ?? 1)
        : // without a placeholder or a value, the span contains a single char
          spanWidth;

    const { snug } = options;

    const width = (spanWidth ?? 1) + spacing + (snug ? 0 : charWidth);

    input.style.width = `${width}px`;

    return width;
  }

  return {
    update,
    destroy() {
      console.log("destroy");
      measurementSpan.remove();
      events.forEach((ev) => input.removeEventListener(ev, handleChange));
      observer.disconnect();
    },
  };
}

const makeObserver = (updateFn: () => void) => {
  return new MutationObserver(function (mutations) {
    updateFn();
  });
};

function createMeasurementSpan() {
  const measurementSpan = document.createElement("span");
  document.body.appendChild(measurementSpan);

  // position it off screen
  measurementSpan.style.position = "absolute";
  measurementSpan.style.top = "-9999px";
  measurementSpan.style.left = "-9999px";

  return measurementSpan;
}
